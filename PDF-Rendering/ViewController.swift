//
//  ViewController.swift
//  PDF-Rendering
//
//  Created by Shailendra Suriyal on 8/15/17.
//  Copyright © 2017 RealDev. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, iCarouselDelegate, iCarouselDataSource {
    
    @IBAction func showInCarousel(_ sender: Any) {
        //Convert pdf into array of images
        
        
        //pass  array of images to the array
        
        
        //reload carousel
        carouselView.reloadData()
    }
    
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        print("shailu t \(numbers.count)")
        if images.count > 0{
            return images.count
        }else{
            return numbers.count
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        numbers = [1,2,3,4,5,6]
        
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        if images.count > 0 {
            tempView.backgroundColor = UIColor.white
            let imageView = UIImageView(image: images[index])
            imageView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
            tempView.addSubview(imageView)
        } else{
            let label = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
            label.setTitle("\(index)", for: .normal)
            label.backgroundColor = UIColor.green
            tempView.addSubview(label)
        }
        return tempView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing {
            return value * 1.1
        }
        return value
    }

    @IBOutlet var carouselView: iCarousel!
    
    @IBOutlet var webView: WKWebView!
    
    var numbers = [Int]()
    var images = [UIImage]()
    
    var flag = true;
    override func loadView() {
        super.loadView()
    }
    
    var pageCount = 0
    
    @IBAction func render(_ sender: UIButton) {
        print("Shailu")
        if (flag){
//        let targetURL = URL(string: "https://www.google.com/")
//        let request = URLRequest(url: targetURL!)
//        webView!.load(request)
//        self.webView!.load(request)
        //UIPrintPageRenderer *renderer = [UIPrintPageRenderer new];
        let renderer = UIPrintPageRenderer();
        renderer.addPrintFormatter(webView.viewPrintFormatter(), startingAtPageAt: 0)
       
        
//        PaperSize *paperSize = [PaperSize getPaperSizeFromPrintOptions:options];
//        float printResolution = paperSize.metric ? kPrintDMM : kPrintDPI;
//
//        float paperWidth = paperSize.width * printResolution;
//        float paperHeight = paperSize.height * printResolution;
//        float topMargin = 0.25f * kPrintDPI;
//        float bottomMargin = 0.25f * kPrintDPI;
//        float leftMargin = 0.25f * kPrintDPI;
//        float rightMargin = 0.25f * kPrintDPI;
        
        let paperRect = CGRect(x: 0, y: 0, width: 400, height: 200)
        let printableRect = CGRect(x: 20,y: 20, width: 400 - 20 - 20,height: 400 - 20 - 20);
        renderer.setValue(paperRect, forKey: "paperRect");
        renderer.setValue(printableRect, forKey: "printableRect")
        //[renderer setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
        //[renderer setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
        
        
        //NSMutableData *pdfData = [NSMutableData data];
        //UIGraphicsBeginPDFContextToData(pdfData, paperRect, nil);
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, paperRect, nil)
        let pages = renderer.numberOfPages;
        renderer.prepare(forDrawingPages: NSMakeRange(0,pages))

        //
//        NSInteger pages = [renderer numberOfPages];
//        [renderer prepareForDrawingPages:NSMakeRange(0, pages)];
        
//        for (NSInteger i = 0; i < pages; i++) {
//            UIGraphicsBeginPDFPage();
//            [renderer drawPageAtIndex:i inRect:printableRect];
//        }
        
        
        for i in pages {
            UIGraphicsBeginPDFPage();
            renderer.drawPage(at: i, in: printableRect)
        }
        
        UIGraphicsEndPDFContext()
       // NSString *renderFile;
       // var renderFile = "Shailu.pdf"
        //[NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"web-%@.pdf", [dateFormat stringFromDate:date]]];
        
        
         do {
            
            try pdfData.write(to: NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("Shailu.pdf")!, options: .atomic)
         }catch{
            print(error)
        }
       // [self deleteTempFile];
        
            
        let pdfDocument: CGPDFDocument  = CGPDFDocument(NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("Shailu.pdf")! as CFURL)!
            self.pageCount = pdfDocument.numberOfPages;
        
         //add pages to images
            
            for i in pages {
                let pageRef = pdfDocument.page(at:(i+1))
                UIGraphicsBeginImageContext(CGSize(width: 200, height: 200))
                 let context = UIGraphicsGetCurrentContext();
//                CGContextTranslateCTM(context, 0.0, size.height);
               // CGContextTranslateCTM(context!, 0.0, 200);
                context?.translateBy(x: 0, y: 200)
//                CGContextScaleCTM(context, 1.0, -1.0);
              //  CGContextScaleCTM(context!, 1.0, -1.0);
                context?.scaleBy(x: 1.0, y: -1.0)
//                CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pageRef, kCGPDFCropBox, CGRectMake(0, 0, size.width, size.height), 0, true));
//
//                CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pageRef, kCGPDFCropBox, CGRectMake(0, 0, size.width, size.height), 0, true));
  
                pageRef?.getDrawingTransform(CGPDFBox.cropBox, rect: CGRect(x: 0, y: 0, width: 400, height: 200), rotate: 0, preserveAspectRatio: true)
                //CGContextDrawPDFPage(context!, pageRef!);
                //context?.drawPDFPage(pageRef!)
               // CGContextDrawPDFPage(context, pageRef);
                context?.drawPDFPage(pageRef!)
//
                let newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                images.append(newImage!)
            }
            
        print("Shailu pc \(self.pageCount)")
            flag = false
        } else{
            print("shshs \(self.pageCount)")
            let filePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("Shailu.pdf")!
            let urlRequest = URLRequest(url: filePath)
            webView.load(urlRequest)
            flag = true
            print("no of images \(images.count)")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.type = iCarouselType.coverFlow
//        var url = NSURL(string:"http://www.kinderas.com/")
//        var req = NSURLRequest(URL:url)
        let targetURL = URL(string: "https://www.apple.com/")
        let request = URLRequest(url: targetURL!)
        self.webView!.load(request)
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension Int: Sequence {
    public func makeIterator() -> CountableRange<Int>.Iterator {
        return (0..<self).makeIterator()
    }
}

